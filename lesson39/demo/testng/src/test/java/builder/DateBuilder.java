package builder;

import bo.DateDeparture;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class DateBuilder {

    public static DateDeparture createDate() {
        Calendar date = new GregorianCalendar();
        date.add(Calendar.DAY_OF_YEAR, 7);
        return new DateDeparture(date);
    }
}
