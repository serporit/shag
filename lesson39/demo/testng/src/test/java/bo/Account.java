package bo;


public class Account {

    private String Login;
    private String Password;

    public Account (String login, String password) {
        Login = login;
        Password = password;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }
}
