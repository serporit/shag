package pages;

import org.openqa.selenium.By;

public class YandexMailDonePage extends AbstractPage {

    private static final By UNREAD_BUTTON_LOCATOR = By.xpath("//a[@data-title='Непрочитанные']");

    public boolean isSendLetterDisplayed() {
        String s = driver.getCurrentUrl();
        return !s.equals("https:\\/\\/mail.yandex.by\\/\\?ncrnd=[\\d]{4}&uid=485081079&login=sashatest2017#compose");
    }

    public YandexMailUnreadPage clickUnreadButton() {
        System.out.println(driver.findElement(UNREAD_BUTTON_LOCATOR).getText());
        driver.findElement(UNREAD_BUTTON_LOCATOR).click();
        return new YandexMailUnreadPage();
    }
}
