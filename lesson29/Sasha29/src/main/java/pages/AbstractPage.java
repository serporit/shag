package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class AbstractPage {

    public static WebDriver driver;

    public AbstractPage(){
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", new File("src\\main\\resources\\chromedriver.exe").getAbsolutePath());
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
    }

    public static void close() {
        driver.close();
    }
}
