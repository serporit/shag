package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 05.04.2017.
 */
public class AbstractPage {
    private static final String URL = "https://mail.yandex.by/";
    public static WebDriver driver;

    public AbstractPage(){
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", new File("src\\main\\resources\\chromedriver.exe").getAbsolutePath());
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
    }

    public void close() {
        driver.close();
    }
}
