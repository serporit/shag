package pages;

import org.openqa.selenium.By;

/**
 * Created by User on 05.04.2017.
 */
public class YandexInboxPage extends AbstractPage{

    private static final By LOGIN_NAME_LOCATOR = By.xpath("//div[@class='mail-User-Name']");

    public boolean isLoginNameDisplayed() {
        return driver.findElement(LOGIN_NAME_LOCATOR).isDisplayed();
    }
}
