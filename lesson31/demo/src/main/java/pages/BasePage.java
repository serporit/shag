package pages;

import org.openqa.selenium.By;

/**
 * Created by Main on 10.04.2017.
 */
public class BasePage extends AbstractPage {
    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@data-key='view=folder&fid=1']");


    public InboxPage openInbox() {
        driver.findElement(INBOX_LINK_LOCATOR).click();
        return new InboxPage();
    }
}
