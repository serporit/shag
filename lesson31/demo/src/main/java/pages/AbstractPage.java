package pages;

import org.openqa.selenium.WebDriver;
import wd.WebDriverWrapper;

/**
 * Created by Account on 05.04.2017.
 */
public class AbstractPage {
    WebDriver driver;

    public AbstractPage() {
        driver = WebDriverWrapper.getInstance();
    }
}
