import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Main on 06.03.2017.
 */
public class Collections {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            list.add(String.valueOf(new Random().nextInt(100)));
        }
        print(list);
        list.add(7, "lucky7");
        print(list);
        list.remove(5);
        list.remove(3);
        list.remove(1);
        print(list);
        System.out.println("Index of lucky element: " + list.indexOf("lucky7"));
        list.clear();
        System.out.println(list.get(8));

        //============================================================================


    }

    private static void print(List<String> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println("\n----------------");

    }

    // https://habrahabr.ru/post/162017/
    // пункты 1-6

    // https://habrahabr.ru/post/128269/

    // https://habrahabr.ru/post/127864/

    //установить maven на домашний компьютер (проверка - косольная команда mvn -version)


}
