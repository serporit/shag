import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Main on 06.03.2017.
 */
public class ClassWithExceptions {
    public static void main(String[] args) throws IOException {
        String content = "";
        File file = new File("src/main/resources/myFile.txt");
        try {
            content = FileUtils.readFileToString(file);
        } catch (IOException e) {
            throw new RuntimeException("File reading error");
        }

        System.out.println(content);
        FileUtils.writeStringToFile(file, "ByeBye!");

        String[] array = new String[10];
        array[0] = "Qwerty";
        array[1] = "Asdfg";
        array[2] = "Zxcvb";
        System.out.println(array[1]);
        System.out.println(array[1].length());
        System.out.println(array[5]);
        System.out.println(array[5].length());
        System.out.println(array[10]);
        System.out.println(array[10].length());
    }
}
