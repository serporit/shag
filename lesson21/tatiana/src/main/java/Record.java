/**
 * Created by User on 20.03.2017.
 */
public class Record {
    private String key;
    private int value;

    public String getKey() {
        return key;
    }

    public int getValue() {
        return value;
    }

    public Record(String key, int value) {
        key = key;
        this.value = value;

    }
}
