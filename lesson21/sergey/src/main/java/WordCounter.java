import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class WordCounter {
    public static void printStatistics(String pathToFolder) throws IOException {
        File inputDir = new File(pathToFolder);
        List<File> fileList = (List<File>) FileUtils.listFiles(inputDir, null, true);
       // System.out.println(fileList); // проверка того, что все файлы записаны в строку
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        String allFilesContent = "";

        for (File file : fileList) {
            String fileContent = FileUtils.readFileToString(file);//записываем в одну строку содержимое каждого файла из 10 строк
            allFilesContent = allFilesContent + fileContent;// записываем в одну большую строку содержание всех файлов+текущий
        }

        String[] words = allFilesContent.split("[«»()\",;\\d\\n\\.\\s-]+");
// заполняем Hashmap словами из большой строки allFilesContent: переменная word последовательно принимает значения из коллекции words от 1-го до последнего
        for (String word : words) {

            if (word.length() > 0) {

                if (map.get(word) == null)// Hashmap пустая, поэтому в нее записывается 1-е слово из строки с индексом 1
                {
                    map.put(word, 1);
                } else {
                    Integer currentCount = map.get(word);// все след. слова записываются последовательно с индексами (value) 2,3,4..
// т.к.ключи (это слова в нашем случае) должны быть уникальны, то когда встречается слово уже имеющееся как ключ, оно записывается"поверх" него, а индекс (value) увелич на 1
                    map.put(word, currentCount + 1);
                }
            }
        }

        ArrayList<Record> recordList = new ArrayList<Record>();// создаем объект - список recordList
        for (String currentKey : map.keySet()) {
            recordList.add(new Record(currentKey, map.get(currentKey)));
        }
// сортировка "пузырьком" ключей (их значений)массива (строки) recordList
        for (int i = 0; i < recordList.size() - 1; i++) {
            for (int j = i + 1; j < recordList.size(); j++) {
                if (recordList.get(j).getValue() > recordList.get(i).getValue()) {
                    Record bubble = recordList.get(i);
                    recordList.set(i, recordList.get(j));
                    recordList.set(j, bubble);
                }
            }
        }

        for (Record record : recordList) {
            System.out.println(record.getValue() + " : " + record.getKey());
        }
    }
}
