import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 20.03.2017.
 */
public class WordCounter {
    public static void printStatistics(String pathToFolder) throws IOException {

        File inputDir = new File(pathToFolder);
        List<File> fileList = (List<File>) FileUtils.listFiles(inputDir, null, true);

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        String[] words;

        for (File file : fileList) {
            String fileContent = FileUtils.readFileToString(file);
            words = fileContent.split("[\uFEFF,\";«»()\\d\\r\\n\\.\\s-]+");
            for (String word : words) {
                if (word.length() > 0) {
                    Integer count = map.get(word);
                    map.put(word, count == null ? 1 : count + 1);
                }
            }
        }
        Record.printSortMap(map);
    }
}