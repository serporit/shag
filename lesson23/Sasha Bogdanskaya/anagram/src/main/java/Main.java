import java.util.Scanner;

/**
 * Created by Vel2 on 26.03.2017.
 */
public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Write the first word:");
        String word1 = sc.next();
        System.out.println("Write the second word:");
        String word2 = sc.next();
        System.out.println();
        System.out.println("First method:");
        Methods.firstMethod(word1, word2);
        System.out.println();
        System.out.println("Second method:");
        Methods.secondMethod(word1, word2);
    }
}
