/**
 * Created by Main on 27.03.2017.
 */
public class Runner {
    public static void main(String[] args) {
        TableEditor.removeColumnFromAllTables("year");

        TableEditor.addColumnToAllTables(3, "run", "50 000", "100 000", "150 000");

        TableEditor.editColumnInAllTables("color", "pink", "rose", "violet");
    }
}
